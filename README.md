# Bugzilla adaptor with TRS support

> **WARNING! This project uses a [pre-release version](https://gitlab.com/kth-mda/lyo/lyo-trs) of Lyo TRS libraries.**

## Getting started


If you are on macOS:

``cp src/main/resources/bugz.properties "/Users/$USER/Library/Application Support/oslc4jbugz/config/oslc4jbugz.properties"``

Run Bugzilla locally:

``docker run -p 80:80 --rm smarx008/bugzilla-dev-lyo``

Using JDK 8:

``mvn clean jetty:run-exploded -Dorg.eclipse.lyo.oslc4j.client.uiuri=http://localhost:8080/ -Dorg.eclipse.lyo.oslc4j.bugzilla.adapter_host=localhost``

Navigate to http://localhost:8080/oslc4jbugzilla/services/catalog//singleton


## License

> Copyright (c) 2023 KTH Royal Institute of Technology
> 
> This program and the accompanying materials are made available under the 
> terms of the Eclipse Public License 2.0 which is available at
> http://www.eclipse.org/legal/epl-2.0.
> 
> SPDX-License-Identifier: EPL-2.0

This project uses code from the Eclipse Lyo project dual-licensed under `EPL-1.0 OR BSD-3-Clause`. We chose to license that code under the terms of `EPL-1.0`.

> Copyright (c) 2012 IBM Corporation and Contributors to the Eclipse Foundation.
> 
>  All rights reserved. This program and the accompanying materials
>  are made available under the terms of the Eclipse Public License v1.0
>  and Eclipse Distribution License v. 1.0 which accompanies this distribution.
>  
>  The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
>  and the Eclipse Distribution License is available at
>  http://www.eclipse.org/org/documents/edl-v10.php.
> 
> SPDX-License-Identifier: EPL-1.0 OR BSD-3-Clause